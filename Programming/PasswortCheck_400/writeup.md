# Programming 400

## Password Check

given file: dumb of 1500 Password hashes

[pwdBin.txt](pwdBin.txt)



----

The dump are sha1 hashes. We run a python script that hash all Rockyou.txt passwords and compare it against the dump. Where was just on equal hash in the dump and from the Rockyou.txt. The password from this was `ncc1701` (also known as USS Enterprise).

```python
import hashlib

f = open("./rockyou.txt","r",encoding='utf-8', errors='ignore')
b = open("./pwdBin.txt","r")
bin = b.read()

a=0
b=10000
k=b''
for name in f.readlines():
    name = name.replace("\n","")
    hash_object = hashlib.sha1(name.encode('utf8'))
    pbHash = hash_object.hexdigest()
    a+=1
    if str(pbHash) in bin:
        print(pbHash + "    " + name)
    if a==b:
        b+=10000
        print(a)

```



 To get the count we put the password into the website [https://haveibeenpwned.com/Passwords](https://haveibeenpwned.com/Passwords)  and get the count how often this Password was pwned .

![Pwned Passwords](./pic/yhbp.png)

 maybe there is a way you can do it direct with the api from the website. But it works for us this way!

 Great Challenge thanks a lot.

# flag{55001_ncc1701}

----

## Team: Hacklabor; Time to solve appr. 2 h

## [hacklabor.de](hacklabor.de)

<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />